package ogr.ushchapovkiy.webmock;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class BaseController {

    private static final String RSA = "RSA";
    private static final Logger LOG = LoggerFactory.getLogger(BaseController.class);

    private KeyPair currentKeyPair;

    private final ObjectMapper mapper;

    @GetMapping
    public String getString(@Nullable @RequestParam Long timeout) throws Exception {

        final StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        if (timeout != null) {
            Thread.sleep(timeout);
        }
        stopWatch.stop();
        LOG.info("Tt=" + stopWatch.getLastTaskTimeMillis() + " ms");
        return "Hello world";
    }

    @PutMapping("/body")
    public ResponseEntity<String> getBody(@RequestParam Integer code) throws Exception {
        return ResponseEntity.status(code).body("Body is here");
    }

    @PostMapping("/keypair")
    public String createKeyPair(@RequestBody KeyPairDto dto) throws Exception {
        this.currentKeyPair = createKeyPair(dto.getPublicKey(), dto.getPrivateKey());
        return "Key pair created";
    }

    @PostMapping("/encryptInstant/string")
    public String encryptInstantString(@RequestBody WrapperStringKey dto) throws Exception {
        if (dto.getPublicKey() == null) {
            return "Public key must be not null";
        }
        if (dto.getData() == null) {
            return "Data must be not null";
        }
        final KeyPair keyPair = createKeyPair(dto.getPublicKey(), dto.getPrivateKey());
        return encrypt(dto.getData(), keyPair.getPublic());
    }

    /**
     * Receive any type of object for encryption. Object will serialize to string, after that string will be encrypted
     * by transmitted public key.
     *
     * @param dto required. Fields dto.publicKey and dto.data is required too.
     */
    @PostMapping("/encryptInstant/object")
    public String encryptInstantObject(@RequestBody WrapperObjectKey dto) throws Exception {
        LOG.info("Received dto={}", dto);
        if (dto.getPublicKey() == null) {
            return "Public key must be not null";
        }
        if (dto.getData() == null) {
            return "Data must be not null";
        }

        final Object data = dto.getData();
        final String s = mapper.writeValueAsString(data);
        LOG.info("String to encryption={}", s);
        final KeyPair keyPair = createKeyPair(dto.getPublicKey(), dto.getPrivateKey());
        return encrypt(s, keyPair.getPublic());
    }

    @PostMapping("/encrypt")
    public String encrypt(@RequestBody Wrapper body) throws Exception {
        if (currentKeyPair == null) {
            return "Must create keyPair first";
        }
        if (body.getData() == null) {
            return "Data must be not null";
        }
        return encrypt(body.getData());
    }

    @PostMapping("/decrypt")
    public String decrypt(@RequestBody Wrapper body) throws Exception {
        final Cipher cipher = getCipher();
        cipher.init(Cipher.DECRYPT_MODE, currentKeyPair.getPrivate());
        final byte[] decrypted = cipher.doFinal(base64StringToBytes(body.getData()));
        return new String(decrypted, StandardCharsets.UTF_8);
    }

    private KeyPair createKeyPair(String publicKey, String privateKey) throws Exception {
        return createKeyPair(base64StringToBytes(publicKey), base64StringToBytes(privateKey));
    }

    private KeyPair createKeyPair(byte[] publicKey, byte[] privateKey) throws InvalidKeySpecException, NoSuchAlgorithmException {
        final KeyFactory keyFactory = getKeyFactory();
        PrivateKey privKey = null;
        if (privateKey != null) {
            final PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(privateKey);
            privKey = keyFactory.generatePrivate(keySpecPKCS8);
        }

        final X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(publicKey);
        final RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(keySpecX509);
        return new KeyPair(pubKey, privKey);
    }

    private KeyFactory getKeyFactory() throws NoSuchAlgorithmException {
        return KeyFactory.getInstance(RSA);
    }

    private Cipher getCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
        return Cipher.getInstance(RSA);
    }

    private byte[] base64StringToBytes(String input) {
        return Optional.ofNullable(input).map(n -> Base64.getDecoder().decode(n)).orElse(null);
    }

    private String encrypt(String primordialData) throws Exception {
        return encrypt(primordialData, null);
    }

    private String encrypt(String primordialData, PublicKey publicKey) throws Exception {
        final byte[] data = primordialData.getBytes(StandardCharsets.UTF_8);
        final Cipher cipher = getCipher();
        final PublicKey key = Optional.ofNullable(publicKey).orElse(getPublicKey());
        cipher.init(Cipher.ENCRYPT_MODE, key);
        final byte[] encrypted = cipher.doFinal(data);
        return Base64.getEncoder().encodeToString(encrypted);
    }

    @Nullable
    private PublicKey getPublicKey() {
        return this.currentKeyPair != null ? this.currentKeyPair.getPublic() : null;
    }
}
