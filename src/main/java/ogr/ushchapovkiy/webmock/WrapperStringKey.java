package ogr.ushchapovkiy.webmock;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WrapperStringKey extends KeyPairDto {
    private String data;
}

