package ogr.ushchapovkiy.webmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebmockApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebmockApplication.class, args);
	}

}
