package ogr.ushchapovkiy.webmock;

import lombok.Data;

@Data
public class Wrapper {
    private String data;
}
