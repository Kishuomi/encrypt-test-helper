package ogr.ushchapovkiy.webmock;

import lombok.Data;

@Data
public class KeyPairDto {
    private String publicKey;
    private String privateKey;
}
