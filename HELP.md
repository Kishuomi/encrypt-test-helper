# Getting Started

### set current public key
curl --location --request POST 'http://localhost:18080/keypair' \
--header 'Content-Type: application/json' \
--data-raw '{
    "publicKey": "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAziFq7IbgfQm/e+vNHH3utNnb2T6jjVBpCLaO3KQVQFOr3dTngpYDGqfzZmyA+5zyGeJlXbgx74rtnWDG5NTEJgU5TSYWa5d3nBouqs0V/HvTv9cK42Z8y9Y8oovU4mQ/ia2tMqs978bAD81ZIeNp/m8Q9QzPgCwCk3QuYMdU9dqxXEXrpRaw/TLFQpKczMHtfxxvav7usIUeuipvjUsCKEPTjuFgpKMX6FW4GzebRdfi1+jYgxk/KyIK0nltMG9vyW+uLCfHGpOs9kXLkH8ga5ABCRkl1Ie3gFeoQbcgfY7tKemK4475ELWyD4sk6ylrnADBGFSe/9PZ8USk3/gxaGjn0wuxjWri+uFSI8DdUxnDf5PYBErIQFArRkGUEjRC54dOu9JHVk8nm6vpEeDIQoFCHaSx0XHthHlTvGuYfvNJ3tggzcBWqoT+Q7lbyxoq+nRaPAgs8+whPVkiVbZ/8IdNfOzOYHF7YuWnkHLwzVu2uc8oScXh/vPpP7GdgDgZ2XZ0yR58wkDomvmvvBGYrCaaAAEU1RU0jtQuA6N9J1bJ/Uixw0b3wmKg86h7aUb+Sch2U3SDBQCRCGU33L9GBzT2HUVdZlOBea0DjAWKrCdDMwUYgmtCll1e5AP4dTm3uIaJ+Rf/VD6TVudbcBMDLlcHydIJ1jU5UBJVZW+Nbt0CAwEAAQ=="
}'


### encrypt by existed public key data transmitted as string, 
curl --location --request POST 'localhost:18080/encrypt' \
--header 'Content-Type: application/json' \
--data-raw '{
    "data": "12345678,1234"
}'


### encrypt by a public key from request, data transmitted as string
curl --location --request POST 'localhost:18080/encryptInstant/string' \
--header 'Content-Type: application/json' \
--data-raw '{
    "publicKey": "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtPgQ/hLVMLAd8vRw8WpfXK0E2DP1PRymzbhjlXJA/PIhoQeriVbvSUumPCCqP6U8XdmM7V2k64jXtSwnflrxRIXTBm/AuK9uHDjMpbA5vruESyDrry4LdUEQjslbHvDKERnMWaxoQ8AqFQ188NDLhICDYS6qw8vLGs6CsOmmqNWc/da/1O2gp0xwuJA1IKcjhPmIyXydCv9m2wp4jRl8KoCOsaQrAK+/ThLdc7n6cPml83pmOfbs+nqZPMStXC3Q5kdCnSqy1kr+99Oje1EYCChG/mxRx24dv224XQaAKZuArL0qJcfOawtpwjOsGNUW1ZPJnFl9G+zu63EaBfQuw1ZsP2zeBcda2OkmnPsJDaUW0DnA6lS3YvKFryJl5e713tX3+Ip6Rx1+mYHpbpXJcKttbXSQ+Vp5ZkD/+saWO1wQKiwWDlrk7D0qlmO1qa15eZHJiwfbfsrjb0Sm8NGeF9fAauUZk4gdBGUqs8RFJau5g3v9o34Bo+osQ1OckKXK5y72C/bgagTM7YRUBzagjgnTXwK7Yauf87awutaJgYnqG1xRiwzPvrcaKCoXPFoaiiC5ONd56/ZtlN/vhw8HKHv7IdAS3AKaP9OtpQ+/tlGDPsfbCicN4fvbJefswm8Kbsn6B2hZI0A39s58v2wIEHVz58RfaJT+yMY8ghgNklkCAwEAAQ==",
    "data": "some string to encryption"
}'


### encrypt by a public key from request, data transmitted as object
curl --location --request POST 'localhost:18080/encryptInstant/object' \
--header 'Content-Type: application/json' \
--data-raw '{
    "publicKey": "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtPgQ/hLVMLAd8vRw8WpfXK0E2DP1PRymzbhjlXJA/PIhoQeriVbvSUumPCCqP6U8XdmM7V2k64jXtSwnflrxRIXTBm/AuK9uHDjMpbA5vruESyDrry4LdUEQjslbHvDKERnMWaxoQ8AqFQ188NDLhICDYS6qw8vLGs6CsOmmqNWc/da/1O2gp0xwuJA1IKcjhPmIyXydCv9m2wp4jRl8KoCOsaQrAK+/ThLdc7n6cPml83pmOfbs+nqZPMStXC3Q5kdCnSqy1kr+99Oje1EYCChG/mxRx24dv224XQaAKZuArL0qJcfOawtpwjOsGNUW1ZPJnFl9G+zu63EaBfQuw1ZsP2zeBcda2OkmnPsJDaUW0DnA6lS3YvKFryJl5e713tX3+Ip6Rx1+mYHpbpXJcKttbXSQ+Vp5ZkD/+saWO1wQKiwWDlrk7D0qlmO1qa15eZHJiwfbfsrjb0Sm8NGeF9fAauUZk4gdBGUqs8RFJau5g3v9o34Bo+osQ1OckKXK5y72C/bgagTM7YRUBzagjgnTXwK7Yauf87awutaJgYnqG1xRiwzPvrcaKCoXPFoaiiC5ONd56/ZtlN/vhw8HKHv7IdAS3AKaP9OtpQ+/tlGDPsfbCicN4fvbJefswm8Kbsn6B2hZI0A39s58v2wIEHVz58RfaJT+yMY8ghgNklkCAwEAAQ==",
    "data": {
        "cardNumber": "0000000000000000",
        "expirationDate": "0221",
        "cardHolder": "IVAN IVANOV",
        "cvv": "101" }
}'



